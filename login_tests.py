""" File with test cases """

import os
import tests.test_data as data
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

driver = webdriver.Chrome(os.path.abspath('chromedriver.exe'))


def correct_login():
    driver.get("https://example_page.com")
    driver.maximize_window()
    wait = WebDriverWait(driver, 10)
    login_button = wait.until(ec.visibility_of_element_located((By.XPATH, "//a[@href='/login']")))
    login_button.click()
    username = wait.until(ec.visibility_of_element_located((By.ID, "user")))
    username.send_keys(data.CORRECT_LOGIN_8_CHAR)
    driver.find_element_by_xpath("//input[@id='password']").send_keys(data.CORRECT_PASSWORD_8_CHAR)
    driver.find_element_by_xpath("//input[@id='login']").click()
    if "Hello, " not in driver.page_source:
        print("Something went wrong")
        driver.quit()
    driver.close()


def incorrect_registration_taken_username():
    driver.get("https://example_page.com")
    driver.maximize_window()
    wait = WebDriverWait(driver, 10)
    sign_up_button = wait.until(ec.visibility_of_element_located((By.XPATH, "//a[@href='/signup']")))
    sign_up_button.click()
    username = wait.until(ec.visibility_of_element_located((By.ID, "email")))
    username.send_keys(data.INCORRECT_EXISTING_LOGIN)
    driver.find_element_by_id("password").send_keys(data.CORRECT_PASSWORD_8_CHAR)
    driver.find_element_by_id("repeat_password").send_keys(data.CORRECT_PASSWORD_8_CHAR)
    driver.find_element_by_id("signup").click()
    if "Username already taken!" not in driver.page_source:
        print("Something went wrong")
        driver.quit()
    driver.close()


# TC_1_1 - Correct login to the application.
correct_login()
# TC_2_2 - Unsuccessful registration to the application with an already taken username.
incorrect_registration_taken_username()
