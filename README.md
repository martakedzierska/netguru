## NETGURU solutions by MK

How to run

1. Install any IDE, e.g. Pycharm Community from https://www.jetbrains.com/pycharm/download/
2. Install Python from https://www.python.org/downloads/
3. Run Python cmd.
4. Type:
pip install selenium
Wait for successfull installation.
5. Open a project in selected IDE.
6. Right-click and "Run"